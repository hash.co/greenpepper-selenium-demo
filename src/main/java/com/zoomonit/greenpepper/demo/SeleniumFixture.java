package com.zoomonit.greenpepper.demo;

import com.greenpepper.annotation.Fixture;
import com.greenpepper.annotation.FixtureMethod;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import javax.inject.Inject;

import java.util.List;

import static java.lang.String.format;

@Fixture(usage = "Selenium")
public class SeleniumFixture {

	private final WebDriver webDriver;

	@Inject
	public SeleniumFixture(WebDriver webDriver) {
		this.webDriver = webDriver;
	}

	@FixtureMethod(usage = "click on")
	public boolean clickOn(String button) throws InterruptedException {
		List<WebElement> buttonsWE = webDriver.findElements(By.tagName("button"));
		for (WebElement buttonWE : buttonsWE) {
			if (StringUtils.equals(buttonWE.getText(), button)) {
				highLight(buttonWE);
				buttonWE.click();
				Thread.sleep(2000L);
				return true;
			}
		}
		Thread.sleep(2000L);
		return false;
	}

	@FixtureMethod(usage = "fill the field with")
	public boolean fillTheFieldWith(String placeholder, String value) throws InterruptedException {
		WebElement input = webDriver.findElement(By.cssSelector(format("input[placeholder=\"%s\"]", placeholder)));
		highLight(input);
		input.sendKeys(value);
		Thread.sleep(2000L);
		return true;
	}

	@FixtureMethod(usage = "open the url")
	public boolean openTheUrl(String url) throws InterruptedException {
		webDriver.get(url);
		Thread.sleep(2000L);
		return true;
	}

	private void highLight(WebElement element) {
		JavascriptExecutor jse = (JavascriptExecutor) webDriver;
		jse.executeScript("arguments[0].style.backgroundColor = 'yellow'", element);
	}
}